
import sys, traceback
import logging
from django.conf import settings
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.utils import simplejson as json

class BaseView:
    """
    This is a common base class for views. It contains generic utility
    methods that are common to dealing with queries/view logic
    """
    
    ALLOWED_HTTP_METHODS = ('get', 'post', 'put', 'delete')
    MEDIA_ROOT_URL = settings.MEDIA_URL[0:len(settings.MEDIA_URL)-1]

    LOGGING_PACKAGE_NAME='bascs.ui'

    INVALID_SESSION_ERROR_MESSAGES = [
                                      'UnAuthorised User',
                                      'You must connect the instance first.',
                                      "User doesn't have permission to perform the requested operation",
                                     ]
    TIMEOUT_MESSAGE = 'Your sesssion has been timed out.<br/>Please sign in again after signing out.'
    TIMEOUT_ERROR_CODE = 401


    def __init__(self):
        module = self.__class__.__module__
        # set the application name
        splitted = module.split('.views.')[0].split('.')
        self.application_name = splitted[len(splitted)-1]
        splitted = module.split('.')
        splitted = splitted[len(splitted)-1].split('_')
        
        self.return_types = []

        # find a template dir        
        template_dir = splitted[0]
        for i in range(1, len(splitted)-1):
            template_dir += '_' + splitted[i]
        self.template_dir = self.application_name + '/' + template_dir

        # find a home url
        self.home_url = self.application_name

        self.domain = ''
        self.logger = logging.getLogger(self.LOGGING_PACKAGE_NAME)
        
        if hasattr(self, 'current_user') and self.current_user:
            self.logger.info('$$$$\n\nthis instance has prev current_user value [%s]\n\n' % (self.current_user))
            self.current_user = None


    def __call__(self, request, *args, **kwargs):

        if request.method.lower() not in self.ALLOWED_HTTP_METHODS:
            raise Http404 #should be 405 ...
        if not hasattr(self, request.method.lower()):
            raise Http404
        
        self.logger.info('HTTP_HOST = %s' % request.META['HTTP_HOST'])

        import datetime
        if 'last_access_utc_time' in request.session:
            last_access_utc_time = request.session['last_access_utc_time']
            self.logger.debug('last_access_utc_time = %s' % last_access_utc_time)
            current_utc_time = datetime.datetime.utcnow()
            diff = current_utc_time - last_access_utc_time
            time_in_seconds = diff.total_seconds()
            self.logger.debug('elapsed time : %s - %s = %d' % (current_utc_time, last_access_utc_time, time_in_seconds))
            print 'elapsed time : %s - %s = %d' % (current_utc_time, last_access_utc_time, time_in_seconds)
            expiry_age = settings.SESSION_EXPIRE_AGE
            if time_in_seconds > expiry_age:
                self.logger.debug('idle time is %d seconds that is more than %d seconds' % (time_in_seconds, expiry_age))
                print 'idle time is %d seconds that is more than %d seconds' % (time_in_seconds, expiry_age)
                if 'current_user' in request.session:
                    request.session['current_user'] = None
                    self.logger.debug('current user info was set to be %s' % (request.session['current_user']))

        self.return_types = [ret_type.strip() for ret_type in request.META["HTTP_ACCEPT"].split(',')]
        #print self.return_types
        self.logger.info('return types = %s' % (self.return_types))
        #print self._is_output_json()

        if not 'current_user' in request.session or request.session['current_user'] == None:

            next_url = request.META['SCRIPT_NAME'] + request.META['PATH_INFO']
            if request.META['QUERY_STRING']:
                next_url += '?%s' % request.META['QUERY_STRING']

            # return json obj if the output type is json
            if self._is_output_json():
                return self.output_json({'error':self.TIMEOUT_MESSAGE, 'error_code':self.TIMEOUT_ERROR_CODE})

            #redirect to the login page
            return self._redirect_to_login_page(request)

        self.current_user = request.session['current_user']
        self.current_user['view_only'] = not self.current_user['admin_role'] and not self.current_user['manager_role'] and not self.current_user['developer_role'] and self.current_user['auditor_role']
        self.session_id = self.current_user['session_id']
        self.logger.info('session id = %s:%s' % (request.session.session_key, self.session_id))
        
        # find domain name of this server
        tokens = request.META['SERVER_NAME'].split('.');
        if len(tokens) < 2:
            self.domain = request.META['SERVER_NAME']
        else:
            start = len(tokens) - 2
            self.domain = '%s.%s' % (tokens[start], tokens[start+1])
        
        # check the login user's permission in this site
        redirect_url = self.redirect_if_not_allowed(request)
        if redirect_url:
            return redirect_url

        # set the default value
        self.session_modified = True
        ret = getattr(self, request.method.lower())(request, *args, **kwargs)

        # reset the session timeout        
        if self.session_modified:
            request.session['last_access_utc_time'] = datetime.datetime.utcnow()
            self.logger.debug('last_access_utc_time is updated to %s' % (request.session['last_access_utc_time']))
            print 'last_access_utc_time is updated to %s' % (request.session['last_access_utc_time'])
        self.logger.debug('For django inernal session management, session expire age is %d' % (request.session.get_expiry_age()))
        self.logger.debug('For django inernal session management, session expires at %s' % (request.session.get_expiry_date()))

        return ret


    def _redirect_to_login_page(self, request):
        next_url = request.META['SCRIPT_NAME'] + request.META['PATH_INFO']
        if request.META['QUERY_STRING']:
            next_url += '?%s' % request.META['QUERY_STRING']
        url = request.META['SCRIPT_NAME'] + '/' + self.application_name + '/login/?next=%s' % next_url
        return self.redirect_by_url(url)


    def __call_without_auth__(self, request, *args, **kwargs):

        if request.method.lower() not in self.ALLOWED_HTTP_METHODS:
            raise Http404 #should be 405 ...
        if not hasattr(self, request.method.lower()):
            raise Http404

        self.return_types = [ret_type.strip() for ret_type in request.META["HTTP_ACCEPT"].split(',')]

        if not 'current_user' in request.session or request.session['current_user'] == None:
            self.user = None
            self.session_id = None
            self.current_user = None
            request.session['current_user'] = None
            self.current_user = {'email_address':'temp'}
        else:
            self.current_user = request.session['current_user']
            self.session_id = self.current_user['session_id']
        
        return getattr(self, request.method.lower())(request, *args, **kwargs)


    def _is_output_json(self):
        for ret_type in self.return_types:
            if ret_type.find('json') >= 0:   return True
        return False


    def redirect_if_not_allowed(self, request):

        self.session_id = None

        import base64
        reason = "This user doesn't have a permission to login"
        message = base64.b64encode('%s;%s' %(self.current_user['email_address'], reason))

        del request.session['current_user']
        next_url = request.META['SCRIPT_NAME'] + request.META['PATH_INFO']
        url = request.META['SCRIPT_NAME'] + '/' + self.application_name + '/login/?next=%s&retry=%s' % (next_url, message)
        return self.redirect_by_url(url)
    

    def get(self, request, *args, **kwargs):
        try:
            if 'json' in request.GET:
                if request.GET['json'] != '':
                    return self.output_json(getattr(self, request.GET['json'])(request, *args, **kwargs))
                else:
                    return self.output_json(self.json(request, *args, **kwargs))

            elif 'action' in kwargs:
                action = kwargs['action']
                del kwargs['action']
                if 'raw' in request.GET:
                    return getattr(self, action)(request, *args, **kwargs)
                template_name = action
                params = getattr(self, action)(request, *args, **kwargs)

            elif 'id' in kwargs:
                template_name = 'show'
                params = self.find(request, *args, **kwargs)

            else:
                template_name = 'list'
                params = self.index(request, *args, **kwargs)

            if 'redirect__url' in params:
                #print 'redirecting to %s' % (params['redirect__url'])
                return self.redirect_by_url(params['redirect__url'])

            # if the returned params is not json, just return the params
            if type(params).__name__ != 'dict':
                return params
            
            if self._is_output_json():
                return self.output_json(params)
            
            if 'page' in request.GET:
                template_name += '.%s.html' % request.GET['page']
            else:
                template_name += '.html'

            params['media__root__url'] = request.META['SCRIPT_NAME'] + self.MEDIA_ROOT_URL           
            params['root__url'] = request.META['SCRIPT_NAME'] + '/' + self.application_name
            params['home__url'] = request.META['SCRIPT_NAME'] + '/' + self.home_url
            params['application__name'] = self.application_name
            params['current_user'] = self.current_user
            params['domain'] = self.domain
            
            return self.output_template(template_name, params)                

        except Exception as inst:
            return self._rethrow_exception(request, inst)


    def post(self, request, *args, **kwargs):
        try:
            if '__output' in request.GET:
                # this is a posted GET action
                return self.get(request, *args, **kwargs)
            if 'action' in kwargs:
                action = kwargs['action']
                del kwargs['action']
                ret = getattr(self, action)(request, *args, **kwargs)
            elif 'id' in kwargs:
                ret = self.update(request, *args, **kwargs)
            else:
                ret = self.create(request, *args, **kwargs)
            if not ret:
                url = request.META['SCRIPT_NAME'] + request.META['PATH_INFO']
                return self.redirect_by_url(url)
            if self._is_output_json():
                return self.output_json(ret)
            return ret
        except Exception as inst:
            return self._rethrow_exception(request, inst)


    def put(self, request, *args, **kwargs):
        try:
            if 'action' in kwargs:
                action = kwargs['action']
                del kwargs['action']
                ret = getattr(self, action)(request, *args, **kwargs)
            else:
                ret = self.update(request, *args, **kwargs)
            if not ret:
                url = request.META['SCRIPT_NAME'] + request.META['PATH_INFO']
                return self.redirect_by_url(url)
            if self._is_output_json():
                return self.output_json(ret)
            return ret
        except Exception as inst:
            return self._rethrow_exception(request, inst)


    def delete(self, request, *args, **kwargs):
        try:
            ret = self.remove(request, *args, **kwargs)
            if not ret:
                url = request.META['SCRIPT_NAME'] + request.META['PATH_INFO']
                return self.redirect_by_url(url)
            if self._is_output_json():
                return self.output_json(ret)
            return ret
        except Exception as inst:
            return self._rethrow_exception(request, inst)


    def _rethrow_exception(self, request, exception):
        """
        Rethrows the original exception we caught. This allows us
        to quickly see the actual line number where the error
        occured
        """
        self.logger.error(self.get_stack_trace_str())
        print str(exception)
        if True in [str(exception).startswith(message) for message in self.INVALID_SESSION_ERROR_MESSAGES]:
            if self._is_output_json():
                return self.output_json({'error':self.TIMEOUT_MESSAGE, 'error_code':self.TIMEOUT_ERROR_CODE})
            return self._redirect_to_login_page(request)
        if self._is_output_json():
            return self.output_json({"error": str(exception)})
        else:
            request.session['error_message'] = '%s' % exception
            self.exc_info = sys.exc_info()
            raise self.exc_info[1], None, self.exc_info[2]


    def get_stack_trace_str(self):
        return str(traceback.format_exception(*sys.exc_info())[1:])
        

    # ### to be implemented
    def index(self, request, *args, **kwargs):
        self.logger.error("GET request[index] is not Implemented")
        raise Exception, "GET request[index] is not Implemented"


    # ### to be implemented
    def new(self, request, *args, **kwargs):
        self.logger.error("NEW request is not Implemented")
        raise Exception, "NEW request is not Implemented"


    # ### to be implemented
    def edit(self, request, *args, **kwargs):
        self.logger.error("EDIT request is not Implemented")
        raise Exception, "EDIT request is not Implemented"


    # ### to be implemented
    def find(self, request, *args, **kwargs):
        self.logger.error("GET request is not Implemented")
        raise Exception, "GET request is not Implemented"


    # ### to be implemented
    def update(self, request, *args, **kwargs):
        self.logger.error("POST request is not Implemented")
        raise Exception, "POST request is not Implemented"


    # ### to be implemented
    def create(self, request, *args, **kwargs):
        self.logger.error("PUT request is not Implemented")
        raise Exception, "PUT request is not Implemented"


    # ### to be implemented
    def remove(self, request, *args, **kwargs):
        self.logger.error("DELETE request is not Implemented")
        raise Exception, "DELETE request is not Implemented"


    ### start of output display methods
    
    def raise_exception(self, request, exception):
        error_message = 'Exception in [%s] : %s\n%s' % (request.method, exception, self.get_stack_trace_str())
        self.logger.error(error_message)
        if self._is_output_json():
            return {'error':'%s' % exception}
        else:
            raise exception


    def output(self, obj):
        if self._is_output_json():
            return self.output_json(obj)
        else:
            return HttpResponse(obj)


    def output_json(self, json_obj):
        # this is for backward compatibilty !!!!!
        # will be removed all inherited view class is modified
        if type(json_obj).__name__ != 'dict':
            return json_obj
        if 'error' not in json_obj:
            json_obj['error'] = None
        return HttpResponse(json.dumps(json_obj), mimetype='application/json')


    def output_xml(self, xml_obj):
        return HttpResponse(xml_obj, mimetype='application/xml')


    def output_template(self, template_name, param_dict):
        template = '%s/%s' % (self.template_dir, template_name)
        return render_to_response(template, param_dict)


    def redirect_by_url(self, url):
        if self._is_output_json():
            # reset the return types in order not to return json
            self.return_types = []
        return HttpResponseRedirect(url)


    def redirect_by_name(self, url_name, args):
        if self._is_output_json():
            # reset the return types in order not to return json
            self.return_types = []
        return HttpResponseRedirect(reverse(url_name, args=args))

    
    def output_exception(self, request_type, exception):
        message = "%s" % (str(exception).replace('Unknown Error ', ''))
        return HttpResponse(message)

    
    def get_session_id(self, request):
        return self.session_id
