
from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from base_view import BaseView

class LoginView(BaseView):
    
    def __init__(self, app_name=None):

        BaseView.__init__(self)
        

    def login(self, request):

        if request.method == 'GET':
            return self._display_login_page(request)

        if request.method != 'POST':
            raise Http404('Only POSTs are allowed')

        if request.POST['username'] == '':
            return self.redirect_login(request, "Please enter Email Address.")
        if request.POST['password'] == '':
            return self.redirect_login(request, "Please enter Password.")

        try:
            self.session_id = None
            if self._authenticate(request):
                if 'next' in request.POST and request.POST['next'] != '':
                    #print 'redirecting to ', request.POST['next']
                    self.logger.info('redirecting to %s', request.POST['next'])
                    return HttpResponseRedirect(request.POST['next'])
                else:
                    return HttpResponseRedirect(request.META['SCRIPT_NAME'] + '/login/')
        except Exception as inst:
            message = "Login Failed [%s]" % inst
            #print message
            self.logger.info(message)
            if str(inst).__contains__("Account locked"):
                return self.redirect_login(request, "Account locked. Kindly reset your password.")                    
                
            return self.redirect_login(request, "Authentication failed. Please try again.")


    def redirect_login(self, request, message=None):

        import base64
        message = base64.b64encode('%s;%s' %(request.POST['username'], message))

        if 'next' in request.POST and request.POST['next'] != '':
            url = request.META['SCRIPT_NAME'] + '/%s/login/?next=%s&retry=%s' %(self.application_name, request.POST['next'], message)
        else:
            url = request.META['SCRIPT_NAME'] + '/%s/login/?retry=%s' %(self.application_name, message)

        #print url
        return HttpResponseRedirect(url)
            

    def logout(self, request):
        try:
            self._logout(request)
        except KeyError:
            pass

        login_url = request.META['SCRIPT_NAME'] + '/login/'

        #print 'login_url = %s' % login_url
        self.logger.info('login_url = %s' % login_url)

        return HttpResponseRedirect(login_url)


    def _display_login_page(self, request):

        template = 'login.html'

        params = {}
        params['media__root__url'] = request.META['SCRIPT_NAME'] + BaseView.MEDIA_ROOT_URL
        params['status'] = 'FAIL'
        params['root__url'] = request.META['SCRIPT_NAME']

        if 'retry' in request.GET:
            try:
                import base64
                tokens = base64.b64decode(request.GET['retry']).split(';')
                self.logger.info('user to retry = %s, %s' % (tokens[0], tokens[1]))
                params['email'] = tokens[0]
                params['msg'] = tokens[1]
            finally:
                pass

        if 'next' in request.GET:
            #print 'next in rendering = %s' % request.GET['next']
            params['next'] = request.GET['next']

        return render_to_response(template, params)


    def _authenticate(self, request):

        #print 'start auth'
        ldap_user_interface = self.get_api_instance(request).get_ldap_user_interface()
        ret_json = ldap_user_interface.login(request.POST['username'], request.POST['password'])
        ret_json['user_details']['session_id'] = ldap_user_interface.session_id
        #print 'ret = %s' % ret_json

        request.session['current_user'] = ret_json['user_details']
        
        import datetime
        request.session['last_access_utc_time'] = datetime.datetime.utcnow()

        return True


    def _logout(self, request):
        try:
            self.session_id = request.session['current_user']['session_id']
            ldap_user_interface = self.get_api_instance(request).get_ldap_user_interface()
            ldap_user_interface.logout()
        except Exception:
            import traceback, sys
            self.logger.info("api failed to logout [%s]" % traceback.format_exception(*sys.exc_info())[1:])
        del request.session['current_user']
        self.current_user = None
