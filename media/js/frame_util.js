﻿
FrameUtil = function(frame_window)
{
    this.f = frame_window;

    var me = this;
    
    me.init = function()
    {
    }

    me.get_height = function()
    {
        var max_height = 0;
        
        if (me.f.document.height)
        {
            max_height = me.f.document.height;
        }
        else if (me.f.document.all)
        {
            if (me.f.document.compatMode && me.f.document.compatMode!= 'BackCompat')
            {
                max_height = me.f.document.documentElement.scrollHeight;
            }
            else
            {
                max_height = me.f.document.body.scrollHeight;
            }
        }
        
        return max_height;
    }

    me.adjust_size = function(min_width, min_height)
    {
        if (typeof min_width == "undefined")
        {
            min_width = 0;
        }
        if (typeof min_height == "undefined")
        {
            min_height = 0;
        }

        if (me.f.document.height)
        {
            //var frame_element = document.getElementById(me.f.name);
            var frame_element = me.f.parent.document.getElementById(me.f.name);
            if(me.f.document.height < min_height)
            {
            	frame_element.style.height = min_height + 'px';
            }
            else
            {
            	frame_element.style.height = me.f.document.height + 'px';
            }
            if(me.f.document.width < min_width)
            {
            	frame_element.style.width = min_width + 'px';
            }
            else
            {
            	frame_element.style.width = me.f.document.width + 'px';
            }
        }
        else if (document.all)
        {
            //var frame_element = document.all[me.f.name];
            var frame_element = me.f.parent.document.all[me.f.name];
            //alert(me.f.document.compatMode);
            if (me.f.document.compatMode && me.f.document.compatMode!= 'BackCompat')
            {
            	if(me.f.document.documentElement.scrollHeight < min_height)
            	{
            		frame_element.style.height = min_height + 'px';
            	}
            	else
            	{
            		frame_element.style.height = me.f.document.documentElement.scrollHeight + 'px';
            	}
                if(me.f.document.documentElement.scrollWidth < min_width)
                {
                	frame_element.style.width = min_width + 'px';
                }
                else
                {
                	frame_element.style.width = me.f.document.documentElement.scrollWidth + 'px';
                }
            }
            else
            {
            	if(me.f.document.body.scrollHeight < min_height)
            	{
            		frame_element.style.height = min_height + 'px';
            	}
            	else
            	{
            		frame_element.style.height = me.f.document.body.scrollHeight + 'px';
            	}
                if(me.f.document.body.scrollWidth < min_width)
                {
                	frame_element.style.width = min_width + 'px';
                }
                else
                {
                	frame_element.style.width = me.f.document.body.scrollWidth + 'px';
                }
            }
        }
        else
        {
            var frame_element = me.f.parent.document.getElementById(me.f.name);
            try
            {
                if(me.f.document.documentElement.scrollHeight < min_height)
                {
                    frame_element.style.height = min_height + 'px';
                }
                else
                {
                    frame_element.style.height = me.f.document.documentElement.scrollHeight + 'px';
                }
                if(me.f.document.documentElement.scrollWidth < min_width)
                {
                    frame_element.style.width = min_width + 'px';
                }
                else
                {
                    frame_element.style.width = me.f.document.documentElement.scrollWidth + 'px';
                }
            }catch(e)
            {}
		}
    }

    me.init();
}



