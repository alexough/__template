﻿
ValueChecker = function()
{
    //this.parent = parent;

    var me = this;
    
    me.init = function()
    {
    }

    me.is_digit = function(val)
    {
        if(val == '')   return false;
        
        var vals = val.split(/(\D)/); 
        //alert(vals.length);
        if(vals.length != 1) return false;
        
        return true;
    }
    
    me.is_alphanumeric = function(val)
    {
        if (val.match(/^[a-zA-Z0-9]+$/))
        {
            return true;
        }
        else
        {
            return false;
        } 
    }
    
    me.is_null = function(val)
    {
        val = val.replace(/^\s+|\s+$/g, "");
        if(val == null || val == '')   return true;
        return false;
    }
    
    me.is_checkbox_checked = function(parent, checkbox_name)
    {
        if(parent.find("input[name='" + checkbox_name + "']:checked").length == 0)  return false;
        return true;
    }
    
    
    
    me.is_duplicated = function(obj_array, index, element_name, element_value)
    {
        for(var i = 0; i < obj_array.length; i++)
        {
            if(i == index)  continue;
            
            //name = me.parent.find("#name").attr('value');
            //if(machine_name == me.obj_array[i].name)
            if(element_value == obj_array[i][element_name])
            {
                return true;
            }
        }
        
        return false;
    }



    ////////////////////////////////////////////////////////////////
    /// methods to validate input data with already existing data ///
    ////////////////////////////////////////////////////////////////
    
    me.validate_in_server = function(url, obj_array, obj, index, request_type)
    {
        if (request_type == null)    request_type = 'GET';
        
        // construct object array with the new/modified values
        var obj_array_str = me.contruct_json_str(obj_array, obj, index);
        
        var string_util = new StringUtil();
        obj_array_str = string_util.url_encode(obj_array_str);
        //alert(obj_array_str);
        
        var message = {'ret':null, 'error':null};

        var data_type = 'html';
        var data = "input=" + obj_array_str + "&time=" + (new Date()).getTime();
        var start_func = null;
        var completion_func = null;
	    $.ajax({
		    type: request_type,
	        url: url,
	        dataType: data_type,
	        data: data,
	        processData: false,
	        async: false,
	        //beforeSend: function() { me.fetch_starting(); },
	        //complete: function(XMLHttpRequest) { me.fetch_completed(); },
	        error: function(XMLHttpRequest, textStatus, errorThrown) { me.failed(XMLHttpRequest, textStatus, errorThrown, message); },
	        success: function(result, textStatus) { me.succeeded(result, textStatus, message); }
	    });
	    
        if(message['error'] != null && message['error'] != '')
        {
            return message['error'];
        }
        
        if(message['ret'] != null && message['ret'] != '')
        {
            return message['ret'];
        }

        return null;
    }
    
    me.contruct_json_str = function(obj_array, obj, index)
    {
    	if(obj_array.length == 0 && obj == null)	return '[]';
    	
        var idx = 0;
        var obj_array_str = '[';

        for( ; idx < index; idx++)
        {
            obj_array_str += JSON.stringify(obj_array[idx]) + ',';
        }
        
        // insert the updated obj if this is for 'edit'
        if(index >= 0)
        {
            obj_array_str += JSON.stringify(obj) + ',';
            idx++;
        }
        
        for(; idx < obj_array.length; idx++)
        {
            obj_array_str += JSON.stringify(obj_array[idx]) + ',';
        }
        
        // append the new obj if this is for 'new'
        if(index < 0 && obj != null)
        {
            obj_array_str += JSON.stringify(obj) + ',';
        }
        
        return obj_array_str.substring(0, obj_array_str.length-1) + ']';
        //return '[' + JSON.stringify(obj) + ']';
    }
    
    me.succeeded = function(result, textStatus, message)
    {
        //alert(result);
        message['ret'] = result.replace(/^\s+|\s+$/g, "");
    }
    
    me.failed = function(XMLHttpRequest, textStatus, errorThrown, message)
    {
        //alert(XMLHttpRequest);
        message['error'] = textStatus;
    }
    
    me.init();

}
