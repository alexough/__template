
DialogScript = function()
{
    this.data_title = 'Confirmation';
    this.wait_title = 'Please Wait...';
    this.message_title = 'Error';
    
    this.message_width = null;

    var me = this;
    
    me.init = function()
    {
        // set the properties of waiting modal dialog
        $("#__div_data_dialog").dialog({
            bgiframe: true,
            autoOpen: false,
            width: 500,
            height: 50,
            //hide: 'slide',
            modal: true
        });

        // set the properties of waiting modal dialog
        $("#__div_wait_dialog").dialog({
            bgiframe: true,
            autoOpen: false,
            width: 320,
            height: 160,
            //hide: 'slide',
            modal: true
        });

        // set the properties of message modal dialog
        $("#__div_message_dialog").dialog({
            bgiframe: true,
            autoOpen: false,
            width: 550,
            height: 160,
            //hide: 'slide',
            modal: true
        });

        $("#__div_data_dialog").find("#__ok_button").click(function(){
            me.confirmed();
        });

        $("#__div_data_dialog").find("#__cancel_button").click(function(){
            $("#__div_data_dialog").dialog('close');
        });

		$("#__div_message_dialog").find("#__close_button").click(function(){
            $("#__div_message_dialog").dialog('close');
        });
    }


	me.get_data = function()
	{
    	return $("#__div_data_dialog");
	}
	
	me.get_wait = function()
	{
    	return $("#__div_wait_dialog");
	}
	
	me.get_message = function()
	{
    	return $("#__div_message_dialog");
	}


	me.set_data_title = function(title)
	{
		me.data_title = title;
	}	

	me.set_wait_title = function(title)
	{
		me.wait_title = title;
	}	

	me.set_message_title = function(title)
	{
		me.message_title = title;
	}


    me.set_message_width = function(width)
    {
    	me.message_width = parseInt(width);
    }


	me.set_data_content = function(contents)
	{
		var content_str = "<br/>";
		for (var i = 0; i < contents.length; i++)
		{
			content_str += contents[i] + "<br/>";
		}
		content_str += "<br/>";
        $("#__div_data_dialog").find("#__div_content").html(content_str);
	}


    me.display_data = function()
    {
        me.display_dialog($("#__div_data_dialog"), me.data_title);
    }
    
    me.display_wait = function()
    {
        me.display_dialog($("#__div_wait_dialog"), me.wait_title);
    }
    
    me.display_message = function(message)
    {
		if (me.message_width != null)
    	{
        	$("#__div_message_dialog").dialog({width:me.message_width});
    	}
        $("#__div_message_dialog").find("#__div_content").html(message);
        me.display_dialog($("#__div_message_dialog"), me.message_title);
    }
    
    me.display_dialog = function(dialog, title)
    {
        dialog.dialog({title:title});
        if(dialog.find("div").length > 0)
        {
           	dialog.dialog({height:dialog.find("div")[0].scrollHeight+100});
        }
        dialog.dialog('open');
    }

	me.alert_message = function(message, title)
	{
		me.set_message_title(title);
		me.display_message(message);
	}


    me.close_data = function()
    {
    	$("#__div_data_dialog").dialog('close');
    }

    me.close_wait = function()
    {
    	$("#__div_wait_dialog").dialog('close');
    }

        
    me.confirmed = function()
    {
    }


	me.register_event_in_message = function(event_hanlder, params)
	{
		$("#__div_message_dialog").find("#__close_button").click(function(){
            $("#__div_message_dialog").dialog('close');
            event_hanlder(params);
        });
	}

    me.init();
}
