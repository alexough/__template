
AjaxRequest = function()
{
	this.action = null;
    this.url = null;
    
    this.data = null;
    this.data_type = 'json';
    this.success_function = null;
    this.failure_function = null;
    
    this.process_data = false;
    this.dialog_script = null;
    
    this.timeout_error_code = 401;
    
    this.customized_before_callback = null;
    this.customized_complete_callback = null;
    this.customized_success_callback = null;
    this.customized_failure_callback = null;
    this.message_dialog_width = null;
    
    var me = this;
    
    me.init = function()
    {
    }
    
    me.initialize = function()
    {
    	if (me.data != null)
    	{
    		me.data['time'] = (new Date()).getTime();
    		me.process_data = true;
    	}

	    if (me.customized_before_callback == null)		me.customized_before_callback = me.before_processing;
	    if (me.customized_complete_callback == null)	me.customized_complete_callback = me.process_completed;
	    if (me.customized_success_callback == null)		me.customized_success_callback = me.process_succeeded;
	    if (me.customized_failure_callback == null)		me.customized_failure_callback = me.process_failed;

    	me.dialog_script = new DialogScript();
    	if (me.message_dialog_width != null)
    	{
			me.dialog_script.set_message_width(me.message_dialog_width);
    	}
    }
    
    me.customize_before_callback = function(customized_callback)
    {
    	me.customized_before_callback = customized_callback;
    }

    me.customize_complete_callback = function(customized_callback)
    {
    	me.customized_complete_callback = customized_callback;
    }

    me.customize_success_callback = function(customized_callback)
    {
    	me.customized_success_callback = customized_callback;
    }

    me.customize_failure_callback = function(customized_callback)
    {
    	me.customized_failure_callback = customized_callback;
    }

    me.customize_message_dialog_width = function(width)
    {
    	me.message_dialog_width = width;
    }
    
    me.register_ok_button_event_handler = function(handler, params)
    {
    	me.dialog_script.register_event_in_message(handler, params);
    }

	me.get = function(url, data, data_type, success_function, failure_function, params)
	{
		me.process('GET', url, data, data_type, success_function, failure_function, params);
	}
	
	me.post = function(url, data, data_type, success_function, failure_function, params)
	{
		me.process('POST', url, data, data_type, success_function, failure_function, params);
	}
	
	me.put = function(url, data, data_type, success_function, failure_function, params)
	{
		me.process('PUT', url, data, data_type, success_function, failure_function, params);
	}
	
	me.remove = function(url, data, data_type, success_function, failure_function, params)
	{
		me.process('DELETE', url, data, data_type, success_function, failure_function, params);
	}

    me.process = function(action, url, data, data_type, success_function, failure_function, params)
    {
		me.action = action;
	    me.url = url;
	    
	    me.data = data || null;
	    me.data_type = data_type || 'json';
	    me.success_function = success_function || null;
	    me.failure_function = failure_function || null;
	    me.params = params;
	    
	    me.initialize();
	    
	    var ajax_dict = {
		    type: me.action,
	        url: me.url,
	        dataType: me.data_type,
	        data: me.data,
	        processData: me.process_data,
	        async: true,
	        beforeSend: function() { me.customized_before_callback(me.params); },
	        complete: function(XMLHttpRequest) { me.customized_complete_callback(me.params); },
	        error: function(XMLHttpRequest, textStatus, errorThrown) { me.customized_failure_callback(XMLHttpRequest, textStatus, errorThrown, me.params); },
	        success: function(result, textStatus) { me.customized_success_callback(result, textStatus, me.params); }
	    }
	    
	    $.ajax(ajax_dict);
    }

    me.before_processing = function()
    {
    	me.dialog_script.display_wait();
    }

    me.process_completed = function()
    {
        me.dialog_script.close_wait();
    }

    me.process_failed = function(XMLHttpRequest, textStatus, errorThrown)
    {
        if(me.failure_function)   me.failure_function(XMLHttpRequest, textStatus, errorThrown, me.params);
		else	me.dialog_script.alert_message(XMLHttpRequest.responseText, 'Error');
    }

    me.process_succeeded = function(result, textStatus)
    {
    	if (me.data_type == 'json' && me.has_error_in_json_result(result))
    	{
			return;
		}
		
    	if (me.data_type != 'json' && me.has_error_in_non_json_result(result))
    	{
    		return;
    	}

		if (me.success_function)
		{
			me.success_function(result, textStatus, me.params);
		}
    }
    
    me.has_error_in_json_result = function(result)
    {
		if (typeof(result['error']) != 'undefined' && result['error'] != '' && result['error'] != null)
		{
			me.dialog_script.alert_message(result['error'], 'Error');			
			if (result['error_code'] == me.timeout_error_code)
			{
				me.dialog_script.register_event_in_message(me.redirect_to_login_page, null);
			}
			return true;
		}

		return false;
    }
    
    me.has_error_in_non_json_result = function(result)
    {
		// if this is a login page, redirect to the login page
		if (result.indexOf('<title>SunGard Enterprise Cloud Infrastructure Service Log In</title>') > 0)
		{
			me.dialog_script.alert_message('Your sesssion has been timed out.<br/>Please sign in again after signing out.', 'Error');			
			me.dialog_script.register_event_in_message(me.redirect_to_login_page, null);
			return true;
		}
		
		return false;
    }
    
    
	me.redirect_to_login_page = function()
	{
		// redirect to the login page
		var redirect_url = $("#__hidden__root__url").val() + '/login/?next=';
		if(window.location != window.parent.window.location)
        {
            window.open(redirect_url + window.parent.window.location, "_parent");
    	}
    	else
    	{
    		window.open(redirect_url + window.location, "_self");
    	}
	}
	
	
	me.alert_message = function(message, title)
	{
		me.dialog_script.alert_message(message, title);
	}


    me.init();
}
