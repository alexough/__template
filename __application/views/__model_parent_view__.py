
from django.db import transaction
from django.utils import simplejson as json
from app_base_view import AppBaseView
from __project__.__application__.models import *

class __Model__View(AppBaseView):

    def index(self, request, __parent___id):

        __parent__s = __Parent__.objects.all().order_by('id')
        __parent__ = __Parent__.objects.get(id=__parent___id)
        __model__s = __parent__.__model___set.all().order_by('id')
        params = {}
        params['__parent__s'] = __parent__s
        params['__parent__'] = __parent__
        params['__model__s'] = __model__s
        return params


    def find(self, request, id):
        __model__ = __Model__.objects.get(id=int(id))
        params = {}
        params['__parent__'] = __model__.__parent__
        params['__model__'] = __model__
        return params


    def new(self, request, __parent___id):
        __parent__ = __Parent__.objects.get(id=__parent___id)
        params = {}
        params['__parent__'] = __parent__
        return params


    def create(self, request, __parent___id=None, id=None):
        if id:
            return self.update(request, id)
        __parent__ = __Parent__.objects.get(id=__parent___id)
        __model__ = __Model__.Create(__parent__, request)
        self.logger.info('created __model__ : %s' % __model__)


    def edit(self, request, id):
        __model__ = __Model__.objects.get(id=int(id))
        params = {'__model__':__model__}
        return params


    def update(self, request, id):
        __model__ = __Model__.Update(id, request)
        self.logger.info('updated __model__ : %s' % __model__)


    def remove(self, request, id):
        __model__ = __Model__.objects.get(id=int(id))
        __model__.delete()

        # when this is called using ajax,
        # return json data type
        return self.output_json({})


    def json(self, request, id):
        __model__ = __Model__.objects.get(id=id)
        self.logger.info('found __model__ : %s' % __model__)
        return __model__.to_json()
    
