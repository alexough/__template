
import logging
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.utils import simplejson as json
from django.conf import settings
from views import BaseView


class AppBaseView(BaseView):

    LOGGING_PACKAGE_NAME='framework.bases'


    def __init__(self):
        BaseView.__init__(self)
        self.home_url += '/';


    def redirect_if_not_allowed(self, request):
        return None
    
    
