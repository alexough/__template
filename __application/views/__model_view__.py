
from django.db import transaction
from django.utils import simplejson as json
from app_base_view import AppBaseView
from __project__.__application__.models import *

class __Model__View(AppBaseView):

    def index(self, request):

        __model__s = __Model__.objects.all().order_by('id')
        params = {'__model__s':__model__s}
        return params


    def find(self, request, id):
        __model__ = __Model__.objects.get(id=int(id))
        params = {'__model__':__model__}
        return params


    def new(self, request):
        return {}


    def create(self, request, id=None):
        if id:
            return self.update(request, id)
        __model__ = __Model__.Create(request)
        self.logger.info('created __model__ : %s' % __model__)


    def edit(self, request, id):
        __model__ = __Model__.objects.get(id=int(id))
        params = {'__model__':__model__}
        return params


    def update(self, request, id):
        __model__ = __Model__.Update(id, request)
        self.logger.info('updated __model__ : %s' % __model__)


    def remove(self, request, id):
        __model__ = __Model__.objects.get(id=int(id))
        __model__.delete()

        # when this is called using ajax,
        # return json data type
        return self.output_json({})


    def json(self, request, id):
        __model__ = __Model__.objects.get(id=id)
        self.logger.info('found __model__ : %s' % __model__)
        return __model__.to_json()
    
