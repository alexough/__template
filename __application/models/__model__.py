
import datetime
from django.db import transaction
from django.db import models
from __project__.__application__.models import *

class __Model__(models.Model):

    # attributes
    __attrs__
    last_modified = models.DateTimeField(blank=False, default=datetime.datetime.now())
    

    # this is necessary for 'syncdb' to recognize this model
    class Meta:
        app_label = '__application__'
        #unique_together = (('', ''),)


    # string representation of this asset
    def __unicode__(self):
        return '%d' % (self.id)


    @classmethod
    def Create(cls, request):
        __model__ = __Model__()
        __model__.store(request)
        return __model__


    @classmethod
    def Update(cls, id, request):
        __model__ = __Model__.objects.get(id=id)
        __model__.store(request)
        return __model__


    def store(self, request):
        __store__
        self.last_modified = datetime.datetime.now()
        self.save()


    def to_json(self):
        ret_json = {}
        ret_json['id'] = self.id
        ret_json['last_modified'] = '%s' % self.last_modified
        __json__
        return ret_json