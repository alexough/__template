
from django.db import transaction
from django.db import models

class __Model__(models.Model):

    __CHOICES = (
        ('F', 'Free'),
        ('A', 'Allocated'),
        ('C', 'Change Requested'),
        ('P', 'In Provisioning'),
        ('S', 'In Service'),
        ('D', 'Deleted'),
    )


    # attributes
    __foreign = models.ForeignKey(__Foreign, blank=False)
    __nullable_fr = models.ForeignKey(__NullFr, blank=True, null=True)
    __one_to_one = models.OneToOneField(__OneToOne)
    __int = models.IntegerField(default=0, unique=True)
    __boolean = models.BooleanField(default=False)
    __string = models.CharField(max_length=10, blank=False, unique=False)
    __nullable_str = models.DateTimeField(blank=True, null=True)
    __choice = models.CharField(max_length=1, choices=__CHOICES, default='F')
    __date_time = models.DateTimeField(blank=False, default=datetime.datetime.now())


    # this is necessary for 'syncdb' to recognize this model
    class Meta:
        app_label = '__application__'
        unique_together = (("", ""),)


    # string representation of this asset
    def __unicode__(self):
        return self.__string + "[" + self.__int + "]"


    @property
    def __choice_in_full(self):
        return self.get___choice_display()


    @classmethod
    def Create(cls, request):
        __model__ = __Model__()
        __model__.store(request)


    @classmethod
    def Update(cls, id, request):
        __model__ = __Model__.objects.get(id=id)
        __model__.store(request)


    def store(self, request):
        self.active = request.POST['active']
        self.street = request.POST['street']
        self.city = request.POST['city']
        self.state = request.POST['state']
        self.zipcode = request.POST['zipcode']
        self.home_phone = request.POST['home_phone']
        self.save()


    def to_json(self):
        ret_json = {}
        ret_json['__'] = self.__
        return ret_json