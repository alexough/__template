
import os
import re
from random import choice
import sys
import shutil
import posixpath

application_dir_name = '__application'

home_dir_holder = '__home_dir__'
project_name_holder = '__project__'
project_path_holder = '__project_path__'
app_name_holder = '__application__'



### from 'django/core/management/commands/startproject.py'
def generate_security_key(directory, project_name, application_name):

    # Create a random SECRET_KEY hash, and put it in the main settings.
    main_settings_file = posixpath.join(directory, project_name, 'settings.py')
    settings_contents = open(main_settings_file, 'r').read()
    fp = open(main_settings_file, 'w')
    secret_key = ''.join([choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for i in range(50)])
    settings_contents = re.sub(r"(?<=SECRET_KEY = ')'", secret_key + "'", settings_contents)

    # replace holders with target values    
    settings_contents = re.sub(project_name_holder, project_name, settings_contents)
    settings_contents = re.sub(app_name_holder, application_name, settings_contents)
    settings_contents = re.sub(project_path_holder, '%s/%s' % (directory, project_name), settings_contents)
    fp.write(settings_contents)
    fp.close()



def update_file(directory, file_name, project_name, application_name):
    file = posixpath.join(directory, file_name)
    file_contents = open(file, 'r').read()
    fp = open(file, 'w')
    file_contents = re.sub(project_name_holder, project_name, file_contents)
    file_contents = re.sub(app_name_holder, application_name, file_contents)
    file_contents = re.sub(project_path_holder, directory, file_contents)
    fp.write(file_contents)
    fp.close()



def update_util_file(directory, file_name, home_dir, project_name, application_name):
    file = posixpath.join(directory, file_name)
    file_contents = open(file, 'r').read()
    fp = open(file, 'w')
    file_contents = re.sub(r"__home_dir__", home_dir, file_contents)
    file_contents = re.sub(project_name_holder, project_name, file_contents)
    file_contents = re.sub(app_name_holder, application_name, file_contents)
    fp.write(file_contents)
    fp.close()




def main():

    if len(sys.argv) != 4:
        print 'usage project.py <home_dir> <project_name> <application_name>'
        sys.exit(0)

    home_dir = sys.argv[1]
    project_name = sys.argv[2]
    application_name = sys.argv[3]

    project_dir = posixpath.join(home_dir, project_name)
    application_dir = posixpath.join(project_dir, application_name)

    template_project_dir = posixpath.join(os.getcwd(), '..')
    template_application_dir = posixpath.join(template_project_dir, application_dir_name)


    try:
        os.chdir(home_dir)
        os.mkdir(project_name)
        print '%s...created' % project_dir

        # move to the project home foler and copy the project files
        os.chdir(project_dir)
        src_file_path = posixpath.join(template_project_dir, '__init__.py')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(project_dir, '__init__.py')

        src_file_path = posixpath.join(template_project_dir, 'manage.py')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(project_dir, 'manage.py')

        src_file_path = posixpath.join(template_project_dir, 'settings.py')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(project_dir, 'settings.py')
        generate_security_key(home_dir, project_name, application_name)
        print '%s...modified' % posixpath.join(project_dir, 'settings.py')

        src_file_path = posixpath.join(template_project_dir, 'urls.py')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(project_dir, 'urls.py')
        update_file(project_dir, "urls.py", project_name, application_name)
        print '%s...modified' % posixpath.join(project_dir, 'urls.py')

        src_file_path = posixpath.join(template_project_dir, 'logging.cfg')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(project_dir, 'logging.cfg')
        update_file(project_dir, "logging.cfg", project_name, application_name)
        print '%s...modified' % posixpath.join(project_dir, 'logging.cfg')

        # create the media folder and copy all files under
        os.chdir(project_dir)
        media_src_dir_path = posixpath.join(template_project_dir, 'media')
        shutil.copytree (media_src_dir_path, 'media')
        print '%s...copied' % posixpath.join(project_dir, 'media')

	# create a view folder and copy all files under
        os.mkdir('views')
        print '%s...created' % posixpath.join(project_dir, 'views')
        view_file_path = '%s/views/__init__.py' % template_project_dir
        shutil.copy(view_file_path, 'views')
        print '%s...copied' % posixpath.join(project_dir, 'views', '__init__.py')
        view_file_path = '%s/views/base_view.py' % template_project_dir
        shutil.copy(view_file_path, 'views')
        print '%s...copied' % posixpath.join(project_dir, 'views', 'base_view.py')
        #update_file(posixpath.join(template_project_dir, 'views'), "base_view.py", project_name, application_name)
        #print '%s...modified' % posixpath.join(application_dir, 'views', 'app_base_view.py')


        # create the templates foler and copy the base template html
        os.chdir(project_dir)
        os.mkdir('templates')
        print '%s...created' % posixpath.join(project_dir, 'templates')
        os.chdir('templates')
        base_html_path = posixpath.join(template_project_dir, 'templates', 'base.html')
        shutil.copy(base_html_path, '.')
        print '%s...copied' % posixpath.join(project_dir, 'templates', 'base.html')

        # create the application template folder        
        os.mkdir(application_name)
        print '%s...created' % posixpath.join(project_dir, 'templates', application_name)

        # initialize log dir
        os.chdir(project_dir)
        os.mkdir('logs')
        print '%s...created' % posixpath.join(project_dir, 'logs')

        # create an application folder & copy files
        os.chdir(project_dir)
        os.mkdir(application_name)
        print '%s...created' % application_dir
        os.chdir(application_name)

        src_file_path = posixpath.join(template_application_dir, '__init__.py')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(application_dir, '__init__.py')

        src_file_path = posixpath.join(template_application_dir, 'urls.py')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(application_dir, 'urls.py')
        update_file(application_dir, "urls.py", project_name, application_name)
        print '%s...modified' % posixpath.join(application_dir, 'urls.py')

        src_file_path = posixpath.join(template_application_dir, 'admin.py')
        shutil.copy(src_file_path, '.')
        print '%s...copied' % posixpath.join(application_dir, 'admin.py')
        update_file(application_dir, "admin.py", project_name, application_name)
        print '%s...modified' % posixpath.join(application_dir, 'admin.py')

        # create the application sub folders
        os.chdir(project_dir)
        os.chdir(application_name)

        os.mkdir('models')
        print '%s...created' % posixpath.join(application_dir, 'models')
        model_file_path = '%s/models/__init__.py' % template_application_dir
        shutil.copy(model_file_path, 'models')
        print '%s...copied' % posixpath.join(application_dir, 'models', '__init__.py')

        os.mkdir('views')
        print '%s...created' % posixpath.join(application_dir, 'views')
        view_file_path = '%s/views/__init__.py' % template_application_dir
        shutil.copy(view_file_path, 'views')
        print '%s...copied' % posixpath.join(application_dir, 'views', '__init__.py')
        view_file_path = '%s/views/app_base_view.py' % template_application_dir
        shutil.copy(view_file_path, 'views')
        print '%s...copied' % posixpath.join(application_dir, 'views', 'app_base_view.py')
        update_file(posixpath.join(application_dir, 'views'), "app_base_view.py", project_name, application_name)
        print '%s...modified' % posixpath.join(application_dir, 'views', 'app_base_view.py')

        os.mkdir('tests')
        print '%s...created' % posixpath.join(application_dir, 'tests')

        os.mkdir('db')
        print '%s...created' % posixpath.join(application_dir, 'db')

	"""
        # copy project util folder & its files
        os.chdir(project_dir)
        util_src_dir_path = posixpath.join(template_project_dir, '__util')
        shutil.copytree(util_src_dir_path, '__util')
        print '%s...copied' % posixpath.join(project_dir, '__util')

        # update project util files        
        update_util_file(posixpath.join(project_dir, '__util'), 'runcmd.bat', home_dir, project_name, application_name)
        print '%s...modified' % posixpath.join(project_dir, '__util', 'runcmd.bat')
        update_util_file(posixpath.join(project_dir, '__util'), 'runserver.bat', home_dir, project_name, application_name)
        print '%s...modified' % posixpath.join(project_dir, '__util', 'runserver.bat')
        update_util_file(posixpath.join(project_dir, '__util'), 'runsyncdb.bat', home_dir, project_name, application_name)
        print '%s...modified' % posixpath.join(project_dir, '__util', 'runsyncdb.bat')

        # copy project util folder & its files
        os.chdir(project_dir)
        os.chdir(application_name)
        util_src_dir_path = posixpath.join(template_application_dir, '_util')
        shutil.copytree (util_src_dir_path, '_util')
        print '%s...copied' % posixpath.join(application_dir, '_util')

        update_util_file(posixpath.join(application_dir, '_util'), 'runpython.bat', home_dir, project_name, application_name)
        print '%s...modified' % posixpath.join(application_dir, '_util', 'runpython.bat')
        update_util_file(posixpath.join(application_dir, '_util'), 'runtestfiles.bat', home_dir, project_name, application_name)
        print '%s...modified' % posixpath.join(application_dir, '_util', 'runtestfiles.bat')
	"""


    except Exception, inst:
        print '%s' % inst


if __name__ == '__main__':
    main()
