
import os
import re
from random import choice
import sys
import shutil
import posixpath

application_dir_name = '__application'


def update_model_file(directory, file_name, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName):

    #parent_name = None
    #ParentName = None
    attr_str = ''
    for attr in attrs:
        '''
        if tokens[1] == 'fr':
            str = '%s = models.ForeignKey(%s, blank=False)' % (tokens[0], tokens[2])
            parent_name = tokens[0]
            ParentName = tokens[2]
        elif tokens[1] == 'one':
            str = '%s = models.OneToOneField(%s, blank=False)' % (tokens[0], tokens[2])
            parent_name = tokens[0]
            ParentName = tokens[2]
        elif tokens[1] == 'int':
            str = '%s = models.IntegerField(default=0, unique=False)' % (tokens[0])
        elif tokens[1] == 'bool':
            str = '%s = models.BooleanField(default=False)' % (tokens[0])
        elif tokens[1] == 'str':
            str = '%s = models.CharField(max_length=%s, blank=False, unique=False)' % (tokens[0], tokens[2])
        elif tokens[1] == 'dt':
            str = '%s = models.DateTimeField(blank=False, default=datetime.datetime.now())' % (tokens[0])
        '''
        attr_str += '    ' + attr


    store_str = ''
    #for tokens in attrs:
    for attr in attrs:
        tokens = attr.split('models.')
        types = tokens[1].split('(')
        if types[0] == 'ForeignKey' or types[0] == 'OneToOneField':
            continue
        field_name = tokens[0].split(' =')[0]
        store_str += "        " + "self.%s = request.POST['%s']" % (field_name, field_name) + '\n'


    json_str = ''
    for attr in attrs:
        tokens = attr.split('models.')
        types = tokens[1].split('(')
        if types[0] == 'ForeignKey' or types[0] == 'OneToOneField':
            continue
        field_name = tokens[0].split(' =')[0]
        json_str += "        " + "ret_json['%s'] = self.%s" % (field_name, field_name) + '\n'


    file = posixpath.join(directory, file_name)
    #print file
    file_contents = open(file, 'r').read()
    #print file_contents
    fp = open(file, 'w')
    file_contents = re.sub(r"__project__", project_name, file_contents)
    file_contents = re.sub(r"__application__", application_name, file_contents)
    file_contents = re.sub(r"__model__", model_name, file_contents)
    file_contents = re.sub(r"__Model__", ModelName, file_contents)
    file_contents = re.sub(r"    __attrs__", '%s' % attr_str, file_contents)
    file_contents = re.sub(r"        __store__", '%s' % store_str, file_contents)
    file_contents = re.sub(r"        __json__", '%s' % json_str, file_contents)

    if parent_name and ParentName:
        file_contents = re.sub(r"__parent__", '%s' % parent_name, file_contents)
        file_contents = re.sub(r"__Parent__", '%s' % ParentName, file_contents)

    #print file_contents
    fp.write(file_contents)
    fp.close()



def update_view_file(directory, file_name, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName):
    '''
    parent_name = None
    ParentName = None
    for tokens in attrs:
        if tokens[1] == 'fr':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
        elif tokens[1] == 'one':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
    '''
    file = posixpath.join(directory, file_name)
    #print file
    file_contents = open(file, 'r').read()
    #print file_contents
    fp = open(file, 'w')
    file_contents = re.sub(r"__project__", project_name, file_contents)
    file_contents = re.sub(r"__application__", application_name, file_contents)
    file_contents = re.sub(r"__model__", model_name, file_contents)
    file_contents = re.sub(r"__Model__", ModelName, file_contents)
    file_contents = re.sub(r"%s_set" % model_name, "%s_set" % model_name.replace('_', ''), file_contents)

    if parent_name and ParentName:
        file_contents = re.sub(r"__parent__", '%s' % parent_name, file_contents)
        file_contents = re.sub(r"__Parent__", '%s' % ParentName, file_contents)

    #print file_contents
    fp.write(file_contents)
    fp.close()




def insert_child_link(directory, file_name, model_name, ModelName, child_name, ChildName):

    file = posixpath.join(directory, file_name)
    fp = open(file, 'a')

    child_script_str = '{% block child_script %}\n'
    child_script_str += '<script type="text/javascript">\n'
    child_script_str += '$(document).ready(function()\n'
    child_script_str += '    {\n'
    child_script_str += '       $(\'<th>%s</th>\').insertBefore("th[id=\'action_th\']");\n' % (ChildName)
    child_script_str += '	    $("td[id^=\'td_show_\']").each(function(){\n'
    child_script_str += '		    $(\'<td><a href="{{root__url}}/%ss/\' + $(this).attr(\'mid\') + \'/%ss/">View</a></td>\').insertBefore(this);\n' %(model_name, child_name)
    child_script_str += '	    });\n'
    child_script_str += '	});\n'
    child_script_str += '</script>\n'
    child_script_str += '{% endblock %}\n'

    fp.write(("".join(child_script_str))+"\n")
    fp.close()




def create_template_list(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName):
    '''
    parent_name = None
    ParentName = None
    for tokens in attrs:
        if tokens[1] == 'fr':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
        elif tokens[1] == 'one':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
    '''
    if parent_name and ParentName:
        file_contents = re.sub(r"__parent_select_starts__", '', file_contents)
        file_contents = re.sub(r"__parent_select_ends__", '', file_contents)
        file_contents = re.sub(r"__parent_select_script_starts__", '', file_contents)
        file_contents = re.sub(r"__parent_select_script_ends__", '', file_contents)
        
        top_menu_str = "<a href='{{root__url}}/%ss/'>%ss</a>" % (parent_name, ParentName)
        top_menu_str += " << <a href='{{root__url}}/%ss/{{%s.id}}/%ss/'>%ss</a>" % (parent_name, parent_name, model_name, ModelName)

    else:
        tokens_1 = file_contents.split('__parent_select_starts__')
        tokens_2 = tokens_1[1].split('__parent_select_ends__')
        tokens_3 = tokens_2[1].split('__parent_select_script_starts__')
        tokens_4 = tokens_3[1].split('__parent_select_script_ends__')
        file_contents = tokens_1[0] + tokens_3[0] + tokens_4[1]
        
        top_menu_str = "<a href='{{root__url}}/%ss/'>%ss</a>" % (model_name, ModelName)

    file_contents = re.sub(r"__top_menu_url__", '%s' % top_menu_str, file_contents)


    show_url = '{{root__url}}/%ss/{{ %s.id }}/' % (model_name, model_name)
    file_contents = re.sub(r"__show_url__", '%s' % show_url, file_contents)
    edit_url = '{{root__url}}/%ss/{{ %s.id }}/edit/' % (model_name, model_name)
    file_contents = re.sub(r"__edit_url__", '%s' % edit_url, file_contents)
    delete_url = '{{root__url}}/%ss/' % (model_name)
    file_contents = re.sub(r"__delete_url__", '%s' % delete_url, file_contents)

    
    file_contents = re.sub(r"__parent__", '%s' % parent_name, file_contents)
    file_contents = re.sub(r"__Parent__", '%s' % ParentName, file_contents)
    file_contents = re.sub(r"__model__", model_name, file_contents)
    file_contents = re.sub(r"__Model__", ModelName, file_contents)


    attr_titles = ''
    '''
    for attr in attrs:
        if attr[1] == 'fr' or attr[1] == 'one':
            continue
        attr_titles += '<th>%s</th>' % (attr[0])
    '''
    for attr in attrs:
        tokens = attr.split('models.')
        types = tokens[1].split('(')
        if types[0] == 'ForeignKey' or types[0] == 'OneToOneField':
            continue
        field_name = tokens[0].split(' =')[0]
        attr_titles += '<th>%s</th>' % (field_name)
    file_contents = re.sub(r"__attr_titles__", attr_titles, file_contents)


    attr_values = ''
    '''
    for attr in attrs:
        if attr[1] == 'fr' or attr[1] == 'one':
            continue
        attr_values += '<td>{{ %s.%s }}</td>' % (model_name, attr[0])
    '''
    for attr in attrs:
        tokens = attr.split('models.')
        types = tokens[1].split('(')
        if types[0] == 'ForeignKey' or types[0] == 'OneToOneField':
            continue
        field_name = tokens[0].split(' =')[0]
        attr_values += '<td>{{ %s.%s }}</td>' % (model_name, field_name)
    file_contents = re.sub(r"__attr_values__", attr_values, file_contents)

    return file_contents


def create_template_new(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName):
    '''
    parent_name = None
    ParentName = None
    for tokens in attrs:
        if tokens[1] == 'fr':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
        elif tokens[1] == 'one':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
    '''
    if parent_name and ParentName:
        hidden_parent_input_str = "<input type='hidden' name='__parent___id' id='__parent___id' value='{{ __parent__.id }}' />"
    else:
        hidden_parent_input_str = ''
    file_contents = re.sub(r"__hidden_parent_input__", hidden_parent_input_str, file_contents)


    file_contents = re.sub(r"__parent__", '%s' % parent_name, file_contents)
    file_contents = re.sub(r"__Parent__", '%s' % ParentName, file_contents)
    file_contents = re.sub(r"__model__", model_name, file_contents)
    file_contents = re.sub(r"__Model__", ModelName, file_contents)


    attr_inputs = ''
    '''
    for attr in attrs:
        if attr[1] == 'fr' or attr[1] == 'one':
            continue
        attr_inputs += '<tr><th>%s</th><td><input type="text" name="%s" id="%s"/></td></tr>' % (attr[0], attr[0], attr[0])
    '''
    for attr in attrs:
        tokens = attr.split('models.')
        types = tokens[1].split('(')
        if types[0] == 'ForeignKey' or types[0] == 'OneToOneField':
            continue
        field_name = tokens[0].split(' =')[0]
        attr_inputs += '<tr><th>%s</th><td><input type="text" name="%s" id="%s"/></td></tr>' % (field_name, field_name, field_name)
    file_contents = re.sub(r"__attr_inputs__", attr_inputs, file_contents)

    return file_contents


def create_template_edit(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName):
    '''
    parent_name = None
    ParentName = None
    for tokens in attrs:
        if tokens[1] == 'fr':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
        elif tokens[1] == 'one':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
    '''
    if parent_name and ParentName:
        hidden_parent_input_str = "<input type='hidden' name='__parent___id' id='__parent___id' value='{{ __parent__.id }}' />"
    else:
        hidden_parent_input_str = ''
    file_contents = re.sub(r"__hidden_parent_input__", hidden_parent_input_str, file_contents)


    file_contents = re.sub(r"__parent__", '%s' % parent_name, file_contents)
    file_contents = re.sub(r"__Parent__", '%s' % ParentName, file_contents)
    file_contents = re.sub(r"__model__", model_name, file_contents)
    file_contents = re.sub(r"__Model__", ModelName, file_contents)


    attr_inputs = ''
    '''
    for attr in attrs:
        if attr[1] == 'fr' or attr[1] == 'one':
            continue
        attr_inputs += '<tr><th>%s</th><td><input type="text" name="%s" id="%s" value="{{ %s.%s }}" /></td></tr>' % (attr[0], attr[0], attr[0], model_name, attr[0])
    '''
    for attr in attrs:
        tokens = attr.split('models.')
        types = tokens[1].split('(')
        if types[0] == 'ForeignKey' or types[0] == 'OneToOneField':
            continue
        field_name = tokens[0].split(' =')[0]
        attr_inputs += '<tr><th>%s</th><td><input type="text" name="%s" id="%s" value="{{ %s.%s }}" /></td></tr>' % (field_name, field_name, field_name, model_name, field_name)
    file_contents = re.sub(r"__attr_inputs__", attr_inputs, file_contents)

    return file_contents


def create_template_show(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName):
    '''
    parent_name = None
    ParentName = None
    for tokens in attrs:
        if tokens[1] == 'fr':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
        elif tokens[1] == 'one':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
    '''
    if parent_name and ParentName:
        parent_str = "<tr><th>__Parent__</th><td align='left'>{{ __parent__.name }}</td></tr><tr><td colspan='2'>&nbsp;</td></tr>"
        back_link_str = "<a href='{{root__url}}/__parent__s/{{ __parent__.id }}/__model__s/' >Back</a>"
    else:
        parent_str = ""
        back_link_str = "<a href='..' >Back</a>"

    file_contents = re.sub(r"__parent_info__", parent_str, file_contents)
    file_contents = re.sub(r"__back_link_url__", back_link_str, file_contents)


    file_contents = re.sub(r"__parent__", '%s' % parent_name, file_contents)
    file_contents = re.sub(r"__Parent__", '%s' % ParentName, file_contents)
    file_contents = re.sub(r"__model__", model_name, file_contents)
    file_contents = re.sub(r"__Model__", ModelName, file_contents)


    attr_values = ''
    '''
    for attr in attrs:
        if attr[1] == 'fr' or attr[1] == 'one':
            continue
        attr_values += '<tr><th>%s</th><td>{{ %s.%s }}</td></tr>' % (attr[0], model_name, attr[0])
    '''
    for attr in attrs:
        tokens = attr.split('models.')
        types = tokens[1].split('(')
        if types[0] == 'ForeignKey' or types[0] == 'OneToOneField':
            continue
        field_name = tokens[0].split(' =')[0]
        attr_values += '<tr><th>%s</th><td>{{ %s.%s }}</td></tr>' % (field_name, model_name, field_name)
    file_contents = re.sub(r"__attr_values__", attr_values, file_contents)

    return file_contents


def update_template_file(directory, file_name, project_dir, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName):

    file = posixpath.join(directory, file_name)
    #print file
    file_contents = open(file, 'r').read()
    #print file_contents
    fp = open(file, 'w')
    file_contents = re.sub(r"__project__", project_name, file_contents)
    file_contents = re.sub(r"__application__", application_name, file_contents)
    file_contents = re.sub(r"__model__", model_name, file_contents)
    file_contents = re.sub(r"__Model__", ModelName, file_contents)

    if file_name == 'list.html':
       file_contents = create_template_list(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName)
    elif file_name == 'new.html':
        file_contents = create_template_new(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName)
    elif file_name == 'edit.html':
        file_contents = create_template_edit(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName)
    elif file_name == 'show.html':
        file_contents = create_template_show(file_contents, project_dir, application_name, model_name, ModelName, attrs, parent_name, ParentName)
    #print file_contents
    fp.write(file_contents)
    fp.close()



def append_line(directory, file_name, line):
    file = posixpath.join(directory, file_name)
    fp = open(file, 'a')
    fp.write(("".join(line))+"\n")
    fp.close()




def append_model_urls(directory, file_name, model_name, ModelName, attrs, parent_name, ParentName):

    '''    
    parent_name = None
    ParentName = None
    for tokens in attrs:
        if tokens[1] == 'fr':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
        elif tokens[1] == 'one':
            parent_name = tokens[0]
            ParentName = tokens[2]
            break
    '''
    if parent_name and ParentName:
        url_str = "\n    (r'^%ss/(?P<%s_id>\d+)/%ss/$', %sView()),\n" % (parent_name, parent_name, model_name, ModelName)
        url_str += "    (r'^%ss/(?P<%s_id>\d+)/%ss/(?P<action>new)/$', %sView()),\n" % (parent_name, parent_name, model_name, ModelName)
    else:
        url_str = "\n    (r'^%ss/$', %sView()),\n" % (model_name, ModelName)
        url_str += "    (r'^%ss/(?P<action>new)/$', %sView()),\n" % (model_name, ModelName)

    url_str += "    (r'^%ss/(?P<id>\d+)/$', %sView()),\n" % (model_name, ModelName)
    url_str += "    (r'^%ss/(?P<id>\d+)/(?P<action>edit)/$', %sView()),\n" % (model_name, ModelName)
    url_str += "    # add new model urls"

    file = posixpath.join(directory, file_name)
    #print file
    file_contents = open(file, 'r').read()
    #print file_contents
    fp = open(file, 'w')
    file_contents = re.sub(r"    # add new model urls", url_str, file_contents)
    #print file_contents
    fp.write(file_contents)
    fp.close()


def main():

    if len(sys.argv) < 5:
        print 'usage model.py home_dir project_name application_name model_name'
        sys.exit(0)

    home_dir = sys.argv[1]
    project_name = sys.argv[2]
    application_name = sys.argv[3]
    model_name = sys.argv[4]

    ModelName = ''
    for token in model_name.split('_'):
        ModelName += token[0].upper() + token[1:]

    '''
    for index in range(5, len(sys.argv)):
        print sys.argv[index]
        tokens = sys.argv[index].split(':')
        attrs.append(tokens)
        if not has_foreign:
            has_foreign = (tokens[1] == 'fr' or tokens[1] == 'one')
    '''

    project_dir = posixpath.join(home_dir, project_name)
    application_dir = posixpath.join(project_dir, application_name)
    models_dir = posixpath.join(application_dir, 'models')
    views_dir = posixpath.join(application_dir, 'views')

    template_project_dir = posixpath.join(os.getcwd(), '..')
    template_application_dir = posixpath.join(template_project_dir, application_dir_name)
    template_template_dir = posixpath.join(template_project_dir, 'templates', '__application')
    template_template_model_dir = posixpath.join(template_project_dir, 'templates', '__application', '__model')

    # read the model definition file
    attrs = []
    has_foreign = False
    parent_name = None
    ParentName = None
    file = posixpath.join(posixpath.join(application_dir, 'db'), model_name+'.db')
    #print file
    fp = open(file, 'r')
    
    while True:
        line = fp.readline()
        if not line or line == '':
            break
        attrs.append(line)
        tokens = line.split('models.')
        types = tokens[1].split('(')
        if not has_foreign:
            has_foreign = (types[0] == 'ForeignKey' or types[0] == 'OneToOneField')
            if has_foreign:
                parent_name = tokens[0].split(' =')[0]
                ParentName = types[1].split(',')[0]
            
    fp.close()
    #print attrs
    #print has_foreign
    #print parent_name, ParentName


    try:
        #directory = os.getcwd()
        #print 'project dir = %s' % directory

        # add a model file along with __init__.py
        os.chdir(models_dir)
        model_file_name = model_name + '.py'
        if has_foreign:
            src_file_path = posixpath.join(template_application_dir, 'models', '__model_parent__.py')
        else:
            src_file_path = posixpath.join(template_application_dir, 'models', '__model__.py')
        shutil.copy(src_file_path, model_file_name)
        print '%s...copied' % posixpath.join(models_dir, model_file_name)
        update_model_file(models_dir, model_file_name, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName)
        print '%s...modified' % posixpath.join(models_dir, model_file_name)
        append_line(models_dir, '__init__.py', 'from %s import %s' % (model_name, ModelName))
        print '%s...modified' % posixpath.join(models_dir, '__init__.py')

        # add a view file along with __init__.py
        os.chdir(views_dir)
        view_file_name = model_name + '_view.py'
        if has_foreign:
            src_file_path = posixpath.join(template_application_dir, 'views', '__model_parent_view__.py')
        else:
            src_file_path = posixpath.join(template_application_dir, 'views', '__model_view__.py')
        shutil.copy(src_file_path, view_file_name)
        print '%s...copied' % posixpath.join(views_dir, view_file_name)
        update_view_file(views_dir, view_file_name, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName)
        print '%s...modified' % posixpath.join(views_dir, view_file_name)
        append_line(views_dir, '__init__.py', 'from %s_view import %sView' % (model_name, ModelName))
        print '%s...modified' % posixpath.join(views_dir, '__init__.py')

        # now template files
        os.chdir(project_dir)
        os.chdir('templates')
        os.chdir(application_name)
        os.mkdir(model_name)
        template_dir = posixpath.join(project_dir, 'templates', application_name, model_name)
        print '%s...created' % template_dir
        
        os.chdir(model_name)
        src_file_path = template_template_model_dir
        shutil.copy(posixpath.join(src_file_path, 'list.html'), '.')
        print '%s...copied' % posixpath.join(template_dir, 'list.html')
        update_template_file(template_dir, 'list.html', project_dir, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName)
        print '%s...modified' % posixpath.join(template_dir, 'list.html')

        shutil.copy(posixpath.join(src_file_path, 'show.html'), '.')
        print '%s...copied' % posixpath.join(template_dir, 'show.html')
        update_template_file(template_dir, 'show.html', project_dir, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName)
        print '%s...modified' % posixpath.join(template_dir, 'show.html')

        shutil.copy(posixpath.join(src_file_path, 'new.html'), '.')
        print '%s...copied' % posixpath.join(template_dir, 'new.html')
        update_template_file(template_dir, 'new.html', project_dir, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName)
        print '%s...modified' % posixpath.join(template_dir, 'new.html')

        shutil.copy(posixpath.join(src_file_path, 'edit.html'), '.')
        print '%s...copied' % posixpath.join(template_dir, 'edit.html')
        update_template_file(template_dir, 'edit.html', project_dir, project_name, application_name, model_name, ModelName, attrs, parent_name, ParentName)
        print '%s...modified' % posixpath.join(template_dir, 'edit.html')

        if parent_name and ParentName:
            directory = '%s/templates/%s/%s' % (project_dir, application_name, parent_name)
            insert_child_link(directory, 'list.html', parent_name, ParentName, model_name, ModelName)
            print '%s...modified' % posixpath.join(project_dir, 'templates', application_name, parent_name, 'list.html')

        # add urls
        append_model_urls(application_dir, 'urls.py', model_name, ModelName, attrs, parent_name, ParentName)    
        print '%s...modified' % posixpath.join(application_dir, 'urls.py')
        
    except Exception, inst:
        print '%s' % inst




if __name__ == '__main__':
    main()