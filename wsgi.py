import os, sys

apache_configuration = os.path.abspath(os.path.dirname(__file__))
project = os.path.dirname(apache_configuration)
sys.path.append(project)


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bascs.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
