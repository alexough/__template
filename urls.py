from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^test_proj/', include('test_proj.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/', include(admin.site.urls)),

    (r'^__application__/', include('__project__.__application__.urls')),                       
)

#url for static files
from django.conf import settings
urlpatterns += patterns('',
	url(r"^%s(?P<path>.*)$" % settings.MEDIA_URL[1:], "django.views.static.serve", { "document_root": settings.MEDIA_ROOT, })
)
