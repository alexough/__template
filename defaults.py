
from django import http
from django.template import Context, RequestContext, loader
from django.conf import settings
from django.shortcuts import render_to_response


def page_not_found(request, template_name='404.html'):
    MEDIA_ROOT_URL = settings.MEDIA_URL[0:len(settings.MEDIA_URL)-1]
    application_path = _find_application_path(request.META['PATH_INFO'])
    #print application_path
    params = {}
    params['media__root__url'] = request.META['SCRIPT_NAME'] + MEDIA_ROOT_URL           
    params['root__url'] = request.META['SCRIPT_NAME'] + application_path
    if 'current_user' in request.session:
        params['user'] = request.session['current_user']
    try:
        params['style'] = settings.CUSTOM_CSS_SETTINGS[request.get_host()]
    except KeyError:
        params['style'] = 'sungard'
    return render_to_response(template_name, params)


def server_error(request, template_name='500.html'):
    MEDIA_ROOT_URL = settings.MEDIA_URL[0:len(settings.MEDIA_URL)-1]
    application_path = _find_application_path(request.META['PATH_INFO'])
    #print application_path
    params = {}
    params['media__root__url'] = request.META['SCRIPT_NAME'] + MEDIA_ROOT_URL           
    params['root__url'] = request.META['SCRIPT_NAME'] + application_path
    if 'current_user' in request.session:
        params['user'] = request.session['current_user']
    if 'error_message' in request.session and request.session['error_message']:
        params['error_message'] = request.session['error_message']
        request.session['error_message'] = None
    try:
        params['style'] = settings.CUSTOM_CSS_SETTINGS[request.get_host()]
    except KeyError:
        params['style'] = 'sungard'
    return render_to_response(template_name, params)


def _find_application_path(path_info):
    index = path_info[1:].find('/')
    if index > 0:
        return path_info[0: index+1]
    else:
        return path_info

